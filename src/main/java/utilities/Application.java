package utilities;

import serializedFeeds.FeedSettingsContainer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


/**
  * Class contains state of application
  * @author Danilyuk Kirill
  * @version 1.0
 */
public class Application implements Runnable{

    /** @see GlobalState */
    private GlobalState appState;
    /** @see CommandListener */
    private CommandListener commandListener;
    /** Field contains executor, which executes user commands */
    private ScheduledThreadPoolExecutor executor;

    /**
     * Constructor creates new object
     */
    public Application() {
        appState = new GlobalState();
        executor = new ScheduledThreadPoolExecutor(10);
        initializeConfiguration();
        commandListener = new CommandListener(new Context(appState, "", executor));

    }

    /**
     * Function initializes configuration of application
     */
    public void initializeConfiguration() {
        try {
            File f = new File("./rssFeedsConfig.cfg");
            if (f.exists() && (!f.isDirectory())) {
                String[] allFeeds = FileReader.readFile("./rssFeedsConfig.cfg", StandardCharsets.UTF_8).split("\n");
                for (String currentFeed: allFeeds) {
                    String[] allParameters = currentFeed.split(" ");
                    FeedSettingsContainer readFeed;
                    if (allParameters.length < 2) {
                            return;
                    }

                    readFeed = new FeedSettingsContainer(allParameters[0],
                            allParameters[1],
                            Long.parseLong(allParameters[2]),
                            new Date(Long.parseLong(allParameters[3])));
                    Integer i = 3;

                    Set<String> keySet = readFeed.getFieldDisplaySettings().keySet();

                    for (String tag : keySet) {
                        if (allParameters[i].toLowerCase().equals("true")) {
                            readFeed.setTagAsSisplayable(tag, true);
                            i++;
                        } else {
                            readFeed.setTagAsSisplayable(tag, false);
                            i++;
                        }
                    }

                    this.appState.addFeed(readFeed);
                    this.executor.scheduleAtFixedRate(readFeed.getWriterThread(), 0, readFeed.getFrequency(), TimeUnit.SECONDS);
                }
            } else {
                FileWriter writer = new FileWriter(f);
                writer.write("");
                writer.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Function runs the application, reads user input,
     * creates threads and handles commands
     */
    @Override
    public void run() {
        Scanner reader = new Scanner(System.in);  // Reading from System.in
        System.out.println("Enter a command. Try help to print list of possible commands.");
        String input = new String();
        while (!input.toLowerCase().equals("exit")) {
            input = reader.nextLine();
            Context currentContext = new Context(appState, input, executor);
            commandListener.handleCommand(currentContext);

        }

        this.executor.shutdown();
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("./rssFeedsConfig.cfg"));
            for (FeedSettingsContainer currentFeed: appState.getAllFeeds()) {
                writer.write(currentFeed.toString() + "\n");
                //Files.write(Paths.get("src/main/resources/rssFeedsConfig.cfg"), (currentFeed.toString() + "\n").getBytes(), StandardOpenOption.CREATE);

            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
