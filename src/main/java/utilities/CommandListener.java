package utilities;

import commands.CommandWithContext;

import java.util.ArrayList;
import java.util.concurrent.Executor;

/**
 * Class for handling commands
 * @see CommandInitializer
 * @author Danilyuk Kirill
 * @version 1.0
 */

public class CommandListener {
    /** Field contains all possible commands */
    private ArrayList<CommandWithContext> allCommands;
    /** Field contains executor for handling commands */
    private Executor executor;

    /**Constructor
     * @param context - context of application
     */
    public CommandListener(Context context) {
        allCommands = new ArrayList<CommandWithContext>();
        CommandInitializer commandInitializer = new CommandInitializer();
        executor = new Executor() {
            @Override
            public void execute(Runnable command) {
                new Thread(command).start();
            }
        };
        commandInitializer.initialise(allCommands, context);
    }

    /** Function handles input command if possible
     * @param currentContext - context with user input
     */
    public void handleCommand(Context currentContext) {
        for (CommandWithContext currentCommand: allCommands) {
            if (currentCommand.checkValidity(currentContext.getUserInput())) {
                currentCommand.setContext(currentContext);
                executor.execute(currentCommand);
            }
        }
    }
}
