package utilities;

import java.util.concurrent.ScheduledThreadPoolExecutor;

/**
 * Class contains data about global state, user input
 * and executor for writing to files
 * @see GlobalState
 * @author Danilyuk Kirill
 * @version 1.0
 */
public class Context {
    GlobalState currentState;
    String userInput;
    ScheduledThreadPoolExecutor exec;


    /**Constructor
     * @param currentState - state of application
     * @param userInput - line which was entered by user
     * @param  exec - command executor
     * */
    public Context(GlobalState currentState, String userInput, ScheduledThreadPoolExecutor exec) {
        this.currentState = currentState;
        this.userInput = userInput;
        this.exec = exec;
    }

    /**Provides access to user input
     * @return string inputed by user
     */
    public String getUserInput() {
        return userInput;
    }

    /**Provides access to current state
     * @return current state of application
     */
    public GlobalState getCurrentState() {
        return currentState;
    }

    /**Provides access to command executor
     * @return executor which executes commands
     */
    public ScheduledThreadPoolExecutor getExec() {
        return exec;
    }
}
