package utilities;

import serializedFeeds.FeedSettingsContainer;

import java.util.ArrayList;

/**
 * Class contains list of all feeds and current feed,
 * available for configuration
 * @author Danilyuk Kirill
 * @version 1.0
 */
public class GlobalState {
    /** List with all feeds
     * @see FeedSettingsContainer
     */
    private  ArrayList<FeedSettingsContainer> allFeeds;

    /** Field contains current feed configuration
     * @see FeedSettingsContainer
     */
    private FeedSettingsContainer currentFeed;

    /** Constructor */
    public GlobalState() {
        this.allFeeds = new ArrayList<>();
        this.currentFeed = null;
    }

    /** Provides access to list of all feeds configuration list
     * @return list with all feed configuration objects
     */
    public synchronized ArrayList<FeedSettingsContainer> getAllFeeds() {
        return allFeeds;
    }

    /** Provides access to current feed configuration
     * @return configuration of current feed
     */
    public synchronized FeedSettingsContainer getCurrentFeed() {
        return currentFeed;
    }

    /** Sets accepted feed as current configuration
     * @param  currentFeed - configration of selected feed
     */
    public synchronized void setCurrentFeed(FeedSettingsContainer currentFeed) {
        this.currentFeed = currentFeed;
    }

    /** Function adds accepted feed to list containing all feed configs
     * @param  feed - adding feed
     */
    public synchronized void addFeed(FeedSettingsContainer feed) {
        this.allFeeds.add(feed);
    }
}
