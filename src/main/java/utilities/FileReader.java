package utilities;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Class allows read from selected
 * @author Danilyuk Kirill
 * @version 1.0
 */
public class FileReader {
    /**
     * Function reads file content as string
     * @param path - path to file
     * @param encoding - file encoding
     * @return string read from file
     */
    public static String readFile(String path, Charset encoding)
            throws IOException
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }
}
