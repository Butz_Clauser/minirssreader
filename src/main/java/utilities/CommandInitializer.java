package utilities;

import commands.*;
import java.util.ArrayList;

/**
 * Class for initializing commands with start context
 * @see Context
 * @author Danilyuk Kirill
 * @version 1.0
 */
public class CommandInitializer {

    /**
     * Function initializes all possible commands with context
     * @param allCommands - list, which will filled by command objects
     * @param context - context of application
     * @see Context
     */
    public void initialise(ArrayList<CommandWithContext> allCommands, Context context){
        allCommands.add(new AddRssCommand(context));
        allCommands.add(new ChangeSelectedFeedConfigurationCommand(context));
        allCommands.add(new PrintAllFeedsCommand(context));
        allCommands.add(new PrintSelectedFeedConfigurationCommand(context));
        allCommands.add(new SelectFeedCommand(context));
        allCommands.add(new RemoveRssCommand(context));
    }
}
