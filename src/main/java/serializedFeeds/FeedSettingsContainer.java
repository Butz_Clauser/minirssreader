package serializedFeeds;

import commands.WriteRssToFileThread;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;

/**
 * Class contains RSS feed configs
 * @see Serializable
 * @author Danilyuk Kirill
 * @version 1.0
 */
public class FeedSettingsContainer implements Serializable {
    /** Field contains path to file, where thread will be saved */
    String pathToSavingFile;

    /** Field contains url of feed */
    String url;

    /** Field contains feed update frequency */
    Long frequency; //update frequency in seconds

    /** Field contains time of last feed update */
    Long lastUpdateTime;

    /** Field contains tags print settings */
    LinkedHashMap<String, Boolean> fieldDisplaySettings;

    /**
     * Field contains thread which writes current feed to file
     * @see WriteRssToFileThread
     */
    WriteRssToFileThread writerThread;

    /** Field contains last pub date for this thread */
    Date lastPubDate;

    /**
     * Constructor
     * @param url - feed url
     * @param pathToSavingFile - path to file, where thread will be saved
     * @param lastUpdateTime - time of last feed update
     * @param lastPubDate - last pub date for this thread
     */
    public FeedSettingsContainer(String url, String pathToSavingFile, Long lastUpdateTime, Date lastPubDate) {
        this.fieldDisplaySettings = new LinkedHashMap<>();
        this.fieldDisplaySettings.put("author", true);
        this.fieldDisplaySettings.put("category", true);
        this.fieldDisplaySettings.put("description", true);
        this.fieldDisplaySettings.put("link", true);
        this.fieldDisplaySettings.put("pubDate", true);
        this.fieldDisplaySettings.put("title", true);
        this.fieldDisplaySettings.put("uri", true);
        this.fieldDisplaySettings.put("contributors", true);
        this.fieldDisplaySettings.put("comments", true);
        this.fieldDisplaySettings.put("contents", true);
        this.fieldDisplaySettings.put("updatedDate", true);

        this.pathToSavingFile = pathToSavingFile;
        this.url = url;
        frequency = 20l;
        this.lastUpdateTime = lastUpdateTime;
        writerThread = new WriteRssToFileThread(this);
        this.lastPubDate = lastPubDate;
    }

    /**
     * Method sets selectes tag ad displayable
     * @param tag - tag name
     * @param isDisplayable - print flag(tag is printed when flag is true)
     * */
    public synchronized void setTagAsSisplayable(String tag, Boolean isDisplayable) {
        this.fieldDisplaySettings.put(tag, isDisplayable);
    }

    /**
     * Provides access to last pub date for this feed
     * @return last pub date for this feed
     */
    public synchronized Date getLastPubDate() {
        return lastPubDate;
    }

    /**
     * Provides access to last pub date for this feed
     * @return last pub date for this feed
     */
    public synchronized void setLastPubDate(Date lastPubDate) {
        this.lastPubDate = lastPubDate;
    }

    /**
     * Provides access to url of this feed
     * @return url of this feed
     */
    public synchronized String getUrl() {
        return this.url;
    }

    /**
     * Provides configuration of current feed as string value for serialization
     * @return configuration of current feed as string value
     */
    public synchronized String printConfiguration() {
        StringBuffer stringContainerValue = new StringBuffer(this.url);
        stringContainerValue.append(' ');
        stringContainerValue.append(this.pathToSavingFile);
        stringContainerValue.append(' ');
        stringContainerValue.append(this.lastUpdateTime);
        stringContainerValue.append(' ');
        stringContainerValue.append(this.lastPubDate);
        for (String currentTag: fieldDisplaySettings.keySet()) {
            stringContainerValue.append(' ');
            stringContainerValue.append(currentTag + ":");
            stringContainerValue.append(this.fieldDisplaySettings.get(currentTag));
        }

        return stringContainerValue.toString();
    }

    /**
     * Provides tags display configuration of current feed
     * @return tags display configuration of current feed
     */
    public synchronized LinkedHashMap<String, Boolean> getFieldDisplaySettings() {
        return fieldDisplaySettings;
    }

    /**
     * Provides configuration of current feed as string value
     * @return configuration of current feed as string value
     */
    @Override
    public String toString() {
        StringBuffer stringContainerValue = new StringBuffer(this.url);
        stringContainerValue.append(' ');
        stringContainerValue.append(this.pathToSavingFile);
        stringContainerValue.append(' ');
        stringContainerValue.append(this.lastUpdateTime);
        stringContainerValue.append(' ');
        if (this.lastPubDate == null) {
            stringContainerValue.append(0);
        } else {
            stringContainerValue.append(this.lastPubDate.getTime());
        }
        for (String currentTag: fieldDisplaySettings.keySet()) {
            stringContainerValue.append(' ');
            stringContainerValue.append(this.fieldDisplaySettings.get(currentTag));
        }

        return stringContainerValue.toString();
    }

    /**
     * Provides thread which writes current feed to file
     * @return thread which writes current feed to file
     */
    public WriteRssToFileThread getWriterThread() {
        return this.writerThread;
    }

    /**
     * Provides feed update frequency
     * @return feed update frequency
     */
    public Long getFrequency() {
        return this.frequency;
    }

    /**
     * Sets path to file, where thread will be saved
     * @param pathToSavingFile - path to file, where thread will be saved
     */
    public void setPathToSavingFile(String pathToSavingFile) {
        this.pathToSavingFile = pathToSavingFile;
    }

    /**
     * Provides path to file, where thread will be saved
     * @return path to file, where thread will be saved
     */
    public String getPathToSavingFile() {
        return pathToSavingFile;
    }

    /**
     * Provides time of last feed update
     * @return time of last feed update
     */
    public Long getLastUpdateTime() {
        return lastUpdateTime;
    }

    /**
     * Method sets time of last feed update
     * @param lastUpdateTime - time of last feed update
     */
    public void setLastUpdateTime(Long lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }
}
