
import utilities.Application;

public class MiniRSSReader {

    static Application application;

    /**
     * Main class
     * @author Danilyuk Kirill
     * @version 1.0
     */
    public static void main(String[] args) {

        application = new Application();
        application.run();
    }
}
