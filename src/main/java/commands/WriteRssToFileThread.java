package commands;

import com.rometools.rome.feed.synd.*;
import com.rometools.rome.io.FeedException;
import com.rometools.rome.io.SyndFeedInput;
import org.apache.commons.validator.routines.UrlValidator;
import serializedFeeds.FeedSettingsContainer;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Date;

/**
 * Class contains method for writing feed content to a file
 * @see Runnable
 * @author Danilyuk Kirill
 * @version 1.0
 */
public class WriteRssToFileThread implements Runnable {


    protected FeedSettingsContainer trackedFeed;
    Boolean feedIsSeen;

    /** Constructor
     * @param trackedFeed - feed which is written by thos thread to file
     */
    public WriteRssToFileThread(FeedSettingsContainer trackedFeed) {

        this.trackedFeed = trackedFeed;
        feedIsSeen = false;
    }

    private void writeToFile() {

        try {
            SyndFeed feed = getFeed(trackedFeed.getUrl());

            if ((feed.getEntries() == null) || (feed.getEntries().isEmpty())) {
                System.out.println("Selected feed not found");
                Thread.currentThread().interrupt();
                Thread.currentThread().join();
                return;
            }

            StringBuffer savedFeed = new StringBuffer();

            Date lastPubDate = null;
            for (SyndEntry currentEntry : feed.getEntries()) {

                if (((trackedFeed.getLastPubDate() != null)
                        && (currentEntry.getPublishedDate().getTime()
                            < trackedFeed.getLastPubDate().getTime()))
                        || (currentEntry.getPublishedDate() == null)) {
                    continue;
                }

                if ((lastPubDate == null) || (lastPubDate.before(currentEntry.getPublishedDate()))) {
                    lastPubDate = currentEntry.getPublishedDate();
                }

                for (String currentProperty : trackedFeed.getFieldDisplaySettings().keySet()) {

                    if (trackedFeed.getFieldDisplaySettings().get(currentProperty)) { //if current property is true, append it to result
                        switch (currentProperty) {
                            case "author":
                                savedFeed.append("author: ");
                                savedFeed.append(currentEntry.getAuthor());
                                savedFeed.append('\n');
                                break;
                            case "category":
                                for (SyndCategory currentCategory : currentEntry.getCategories()) {
                                    savedFeed.append("category: \n");
                                    savedFeed.append("    name: ");
                                    savedFeed.append(currentCategory.getName() + "\n");
                                    savedFeed.append("    taxonomyUri:");
                                    savedFeed.append(currentCategory.getTaxonomyUri() + "\n");
                                    savedFeed.append('\n');
                                }
                                break;
                            case "description":
                                savedFeed.append("description: ");
                                savedFeed.append(currentEntry.getDescription());
                                savedFeed.append('\n');
                                break;
                            case "link":
                                savedFeed.append("link: ");
                                savedFeed.append(currentEntry.getLink());
                                savedFeed.append('\n');
                                break;
                            case "pubDate":
                                savedFeed.append("pubDate: ");
                                savedFeed.append(currentEntry.getPublishedDate());
                                savedFeed.append('\n');
                                break;
                            case "title":
                                savedFeed.append("title: ");
                                savedFeed.append(currentEntry.getTitle());
                                savedFeed.append('\n');
                                break;
                            case "uri":
                                savedFeed.append("uri: ");
                                savedFeed.append(currentEntry.getUri());
                                savedFeed.append('\n');
                                break;
                            case "contributors":
                                for (SyndPerson currentContributor : currentEntry.getContributors()) {
                                    savedFeed.append("contributors: ");
                                    savedFeed.append("    email: ");
                                    savedFeed.append(currentContributor.getEmail() + "\n");
                                    savedFeed.append("    name: ");
                                    savedFeed.append(currentContributor.getName() + "\n");
                                    savedFeed.append("    uri: ");
                                    savedFeed.append(currentContributor.getUri() + "\n");
                                    savedFeed.append('\n');
                                }
                                break;
                            case "comments":
                                savedFeed.append("comments: ");
                                savedFeed.append(currentEntry.getComments());
                                savedFeed.append('\n');
                                break;
                            case "contents":
                                savedFeed.append("contents: ");
                                for (SyndContent currentContent : currentEntry.getContents()) {
                                    savedFeed.append("    mode:");
                                    savedFeed.append(currentContent.getMode());
                                    savedFeed.append("    type:");
                                    savedFeed.append(currentContent.getType());
                                    savedFeed.append("    value:");
                                    savedFeed.append(currentContent.getValue());
                                    savedFeed.append('\n');
                                }
                                break;
                            case "updatedDate":
                                savedFeed.append("updatedDate: ");
                                savedFeed.append(currentEntry.getUpdatedDate());
                                savedFeed.append('\n');
                        }
                    }
                }
            }

            trackedFeed.setLastPubDate(lastPubDate);

            if (!Files.exists(Paths.get(trackedFeed.getPathToSavingFile()))) {
                Files.write(Paths.get(trackedFeed.getPathToSavingFile()), (savedFeed.toString() + "\n").getBytes(), StandardOpenOption.CREATE);
            } else {
                if (feedIsSeen) {
                    Files.write(Paths.get(trackedFeed.getPathToSavingFile()), (savedFeed.toString() + "\n").getBytes(), StandardOpenOption.APPEND);
                }
            }

            feedIsSeen = true;

        } catch (MalformedURLException e) {
            e.printStackTrace();
            //return;
        } catch (IOException e) {
            e.printStackTrace();
            //return;
        } catch (FeedException e) {
            e.printStackTrace();
            //return;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private void tryToWrite() {
        Long currentTime = System.currentTimeMillis();

        if ((currentTime - trackedFeed.getLastUpdateTime()) > trackedFeed.getFrequency()) {
            trackedFeed.setLastUpdateTime(System.currentTimeMillis());
            writeToFile();
        }
    }

    /**
     * @see Runnable
     */
    @Override
    public void run() {
            tryToWrite();
    }

    private SyndFeed getFeed(String input) throws MalformedURLException, IOException, FeedException{
        InputStream io;
        UrlValidator validator = new UrlValidator();
        SyndFeed feed;
        SyndFeedInput syndFeedInput = new SyndFeedInput();
        if (validator.isValid(input)) {
            URL url = new URL(input);
            feed = syndFeedInput.build(new InputStreamReader(url.openStream()));
        } else if (Files.exists(Paths.get(input))) {
            File file = new File(input);
            feed = syndFeedInput.build(file);
            /*Files.readAllBytes(Paths.get(input));
            io = file to io;*/
        } else {
            feed = null;
        }
        //SyndFeedInput input = new SyndFeedInput();
        //SyndFeed feed = input.build(new InputStreamReader(io));
        return feed;
    }
}
