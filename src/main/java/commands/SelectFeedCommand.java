package commands;

import serializedFeeds.FeedSettingsContainer;
import utilities.Context;
import utilities.GlobalState;

/**
 * Class contains method for selecting RSS feed
 * @see Command
 * @author Danilyuk Kirill
 * @version 1.0
 */
public class SelectFeedCommand extends CommandWithContext {

    /**
     * Constructor
     * @param context - current context of application
     */
    public SelectFeedCommand(Context context) {
        super(context);
    }

    /**
     * @see Command
     */
    @Override
    public Boolean checkValidity(String command) {
        if (command == null) {
            return false;
        }

        String[] commandParts = command.split(" ", 2);

        if ((commandParts.length != 2) || (!commandParts[0].equals("sf")) || (commandParts[1].equals(""))) {
            return false;
        }
        return true;
    }

    /**
     * @see Command
     */
    @Override
    public void handleCommand(Context currentContext) {
        String command = currentContext.getUserInput();
        GlobalState currentState = currentContext.getCurrentState();
        String[] commandArguments = command.split(" ", 2);
        for (FeedSettingsContainer currentFeed: currentState.getAllFeeds()) {
            if (currentFeed.getUrl().toLowerCase().equals(commandArguments[1])) {
                currentState.setCurrentFeed(currentFeed);
                return;
            }
        }
    }

    /**
     * @see Command
     */
    @Override
    public void setContext(Context context) {
        this.context = context;
    }

    /**
     * @see Runnable
     */
    @Override
    public void run() {
        handleCommand(this.context);
    }
}
