package commands;

import utilities.Context;

/**
 * Interface contsins essential command method
 * @see Command
 * @see Context
 * @author Danilyuk Kirill
 * @version 1.0
 */
public interface Command {
    /** Function checks validity of command
     * @param command - inputed command
     * @return result of validity check(true matches valid command)
     */
    Boolean checkValidity(String command);

    /** Function handles command
     * @param context - current application context
     */
    void handleCommand(Context context);

    /** Function sets current context for this command
     * @param context - current application contrxt
     */
    void setContext(Context context);
}
