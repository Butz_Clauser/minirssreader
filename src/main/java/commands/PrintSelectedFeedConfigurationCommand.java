package commands;

import utilities.Context;
import utilities.GlobalState;

/**
 * Class contains method for printing configs of selected RSS feed
 * @see Command
 * @author Danilyuk Kirill
 * @version 1.0
 */
public class PrintSelectedFeedConfigurationCommand extends CommandWithContext {

    /**
     * Constructor
     * @param context - current context of application
     */
    public PrintSelectedFeedConfigurationCommand(Context context) {
        super(context);
    }

    /**
     * @see Command
     */
    @Override
    public void setContext(Context context) {
        this.context = context;
    }

    /**
     * @see Command
     */
    @Override
    public Boolean checkValidity(String command) {
        if (command == null) {
            return false;
        }

        if ((command.length() != 4) || (!command.equals("psfc"))) {
            return false;
        }

        return true;
    }

    /**
     * @see Command
     */
    @Override
    public void handleCommand(Context currentContext) {
        GlobalState currentState = currentContext.getCurrentState();
        if (currentState.getCurrentFeed() == null) {
            System.out.println("Current feed not selected");
        } else {
            System.out.println(currentState.getCurrentFeed().printConfiguration());
        }
    }

    /**
     * @see Runnable
     */
    @Override
    public void run() {
        handleCommand(this.context);
    }
}
