package commands;

import utilities.Context;
import utilities.GlobalState;

/**
 * Class contains method for changing selected feed configuration
 * @see Command
 * @author Danilyuk Kirill
 * @version 1.0
 */
public class ChangeSelectedFeedConfigurationCommand extends CommandWithContext {

    /**
     * Constructor
     * @param context - current context of application
     * */
    public ChangeSelectedFeedConfigurationCommand(Context context) {
        super(context);
    }

    /**
     * @see Command
     */
    @Override
    public void setContext(Context context) {
        this.context = context;
    }

    /**
     * @see Command
     */
    @Override
    public Boolean checkValidity(String command) {
        if (command == null) {
            return false;
        }

        String[] commandParams = command.split(" ");

        if ((commandParams.length < 3) || (!commandParams[0].equals("csfc"))) {
            return false;
        }

        return true;
    }

    /**
     * @see Command
     */
    @Override
    public void handleCommand(Context context) {
        GlobalState currentState = context.getCurrentState();
        String userInput = context.getUserInput();
        if (currentState.getCurrentFeed() == null) {
            return;
        }
        String[] commandParams = userInput.split(" ");

        if (commandParams[1].equals("savingFileName")
                && (commandParams.length >= 3)
                && (commandParams[2] != null)
                && (!commandParams[2].equals(""))) {
            currentState.getCurrentFeed().setPathToSavingFile(commandParams[2]);
        }

        for (int i = 1; i < commandParams.length; i += 2) {
            if ((i + 1) < commandParams.length) {
                if (currentState.getCurrentFeed().getFieldDisplaySettings().containsKey(commandParams[i])) {
                    switch (commandParams[i + 1]) {
                        case "true":
                            currentState.getCurrentFeed().getFieldDisplaySettings().put(commandParams[i], true);
                            break;
                        case "false":
                            currentState.getCurrentFeed().getFieldDisplaySettings().put(commandParams[i], false);
                            break;
                    }
                }
            }
        }
    }

    /**
     * @see Runnable
     */
    @Override
    public void run() {
        handleCommand(this.context);
    }
}
