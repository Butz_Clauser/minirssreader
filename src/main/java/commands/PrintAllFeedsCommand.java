package commands;

import serializedFeeds.FeedSettingsContainer;
import utilities.Context;

/**
 * Class contains method for printing all RSS feed's urls
 * @see Command
 * @author Danilyuk Kirill
 * @version 1.0
 */
public class PrintAllFeedsCommand extends CommandWithContext {

    /**
     * Constructor
     * @param context - current context of application
     * */
    public PrintAllFeedsCommand(Context context) {
        super(context);
    }

    /**
     * @see Command
     */
    @Override
    public Boolean checkValidity(String command) {
        if (command == null) {
            return false;
        }

        if ((command.length() != 3) || (!command.equals("paf"))) {
            return false;
        }
        return true;
    }

    /**
     * @see Command
     */
    @Override
    public void handleCommand(Context currentContext) {
        for (FeedSettingsContainer currentFeed: currentContext.getCurrentState().getAllFeeds()) {
            System.out.println(currentFeed.getUrl());
        }
    }

    /**
     * @see Command
     */
    @Override
    public void setContext(Context context) {
        this.context = context;
    }

    /**
     * @see Runnable
     */
    @Override
    public void run() {
        handleCommand(this.context);
    }
}
