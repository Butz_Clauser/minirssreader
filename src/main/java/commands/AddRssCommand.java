package commands;

import org.apache.commons.validator.routines.UrlValidator;
import serializedFeeds.FeedSettingsContainer;
import utilities.Context;
import utilities.FileReader;
import utilities.GlobalState;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.TimeUnit;

/**
 * Class contains method for adding another RSS feed to track
 * @see Command
 * @author Danilyuk Kirill
 * @version 1.0
 */

public class AddRssCommand extends CommandWithContext {

    /**
     * Constructor
     * @param context - current context of application
     */
    public AddRssCommand(Context context) {
        super(context);
    }

    /**
     * @see Command
     */
    @Override
    public void setContext(Context context) {
        this.context = context;
    }

    /**
     * @see Command
     */
    @Override
    public Boolean checkValidity(String command) {
        if (command == null) {
            return false;
        }

        String[] commandParts = command.split(" ", 3);

        if ((commandParts.length < 2)
                || (!commandParts[0].equals("ar"))
                || (commandParts[1].equals(""))) {
            return false;
        }

        return true;
    }

    private Boolean urlIsValid(String url) {
        UrlValidator urlValidator = new UrlValidator();
        return urlValidator.isValid(url);
    }

    /**
     * @see Command
     */
    @Override
    public void handleCommand(Context currentContext) {
        String command = currentContext.getUserInput();
        GlobalState currentState = currentContext.getCurrentState();
        String[] commandParts = command.split(" ", 3);
        try {
            if (!urlIsValid(commandParts[1])) {
                System.out.println("Url is not valid");
                return;
            }
            /*URL feedUrl = new URL(commandParts[1]);
            SyndFeedInput input = new SyndFeedInput();
            SyndFeed feed = input.build(new InputStreamReader(feedUrl.openStream()));
            */
            if (isUrlUnique(commandParts[1])) {
                FeedSettingsContainer newFeed;
                if ((commandParts.length < 3) || (commandParts[2].equals(""))) {
                    String defaultFileName = new String(commandParts[1]);
                    newFeed = new FeedSettingsContainer(commandParts[1],
                                                        defaultFileName
                                                            .replaceAll("([\\\\/:.])",
                                                                    "_"),
                                                        System.currentTimeMillis(),
                                                        null);
                } else {
                    newFeed = new FeedSettingsContainer(commandParts[1],
                            commandParts[2],
                            System.currentTimeMillis(),
                            null);
                }

                currentState.addFeed(newFeed);
                currentState.setCurrentFeed(newFeed);

                writeConfigToFile(newFeed);

                context.getExec().scheduleAtFixedRate(newFeed.getWriterThread(), 0, newFeed.getFrequency(), TimeUnit.MILLISECONDS);
            }
        }  catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } /*catch (FeedException e) {
            e.printStackTrace();
            System.out.println("Wrong URL format");
        }*/
    }

    private synchronized void writeConfigToFile(FeedSettingsContainer newFeed) throws IOException {
        if (!Files.exists(Paths.get("./rssFeedsConfig.cfg"))) {
            Files.write(Paths.get("./rssFeedsConfig.cfg"), (newFeed.toString() + "\n").getBytes(), StandardOpenOption.CREATE);
        } else {
            Files.write(Paths.get("./rssFeedsConfig.cfg"), (newFeed.toString() + "\n").getBytes(), StandardOpenOption.APPEND);
        }
    }

    private Boolean isUrlUnique(String url) {
        try {
            String fileContent =
                    FileReader.readFile(
                            "./rssFeedsConfig.cfg",
                            StandardCharsets.UTF_8
                    );

            String[] allUrls = fileContent.split("\n");
            for (String currentUrl: allUrls) {
                if (currentUrl.split(" ")[0].equals(url)) {
                    return false;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * @see Runnable
     */
    @Override
    public void run() {
        handleCommand(this.context);
    }
}
