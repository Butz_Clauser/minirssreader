package commands;

import utilities.Context;

/**
 * Class contains method and fields of any command
 * @see Command
 * @author Danilyuk Kirill
 * @version 1.0
 */
public abstract class CommandWithContext implements Command, Runnable {

    /** Field contains current application context */
    Context context;

    /** Constructor
     * @param context - current application context
     */
    public CommandWithContext(Context context) {
        this.context = context;
    }

    /** Provides access to context of command */
    public Context getContext() {
        return context;
    }

    /**
     * @see Runnable
     */
    public abstract void run();
}
