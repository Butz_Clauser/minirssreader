import commands.WriteRssToFileThread;
import org.junit.Before;
import org.junit.Test;
import serializedFeeds.FeedSettingsContainer;
import utilities.GlobalState;
import static org.junit.Assert.assertEquals;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Date;

import static java.nio.file.Files.readAllBytes;

public class MiniRssTest {
    private GlobalState currentState;
    private FeedSettingsContainer firstFeed;
    private FeedSettingsContainer secondFeed;
    private WriteRssToFileThread rssWriter;

    @Before
    public void setContext() {
        currentState = new GlobalState();
        try {
            FileWriter fwOb = new FileWriter("./testResult3", false);
            PrintWriter pwOb = new PrintWriter(fwOb, false);
            pwOb.flush();
            pwOb.close();
            fwOb.close();
            Files.write(Paths.get("./testResult3"), "".getBytes(), StandardOpenOption.DELETE_ON_CLOSE);
        } catch (IOException e) {

        }
    }

    @Test
    public void checkToSingleFileWriting(){

        firstFeed = new FeedSettingsContainer("./textRss.txt", "./testResult", 0l, null);
        rssWriter = new WriteThreadWithCounter(firstFeed);
        rssWriter.run();
        rssWriter.run();
        try {
            String readFile = new String(Files.readAllBytes(Paths.get("./testResult")));
            String mustBe = new String (Files.readAllBytes(Paths.get("./mustBe")));
            assertEquals(mustBe.replaceAll("\n", "").replaceAll("\r", ""), readFile.replaceAll("\n", "").replaceAll("\r", ""));
            //Files.write(Paths.get("testResult"), new String().getBytes(), StandardOpenOption.WRITE);
            FileWriter fwOb = new FileWriter("./testResult", false);
            PrintWriter pwOb = new PrintWriter(fwOb, false);
            pwOb.flush();
            pwOb.close();
            fwOb.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void checkToPeriodicalSingleFileWriting(){

        firstFeed = new FeedSettingsContainer("./textRss.txt", "./testResult2", 0l, null);
        rssWriter = new WriteThreadWithCounter(firstFeed);
        rssWriter.run();
        assertEquals(true, Files.exists(Paths.get("./testResult2")));
        try {
            String readFile = new String(Files.readAllBytes(Paths.get("./testResult2")));
            String mustBe = new String (Files.readAllBytes(Paths.get("./mustBe2")));
            assertEquals(mustBe, readFile);

            firstFeed.setLastPubDate(new Date(0));
            rssWriter.run();
            readFile = new String(Files.readAllBytes(Paths.get("./testResult2")));
            readFile = readFile.replaceAll("\n", "").replaceAll("\r", "").replaceAll(" ", "");
            mustBe = new String (Files.readAllBytes(Paths.get("./mustBe3")));
            mustBe = mustBe.replaceAll("\n", "").replaceAll("\r", "").replaceAll(" ", "");

            assertEquals(mustBe, readFile);
            //Files.write(Paths.get("testResult"), new String().getBytes(), StandardOpenOption.WRITE);
            FileWriter fwOb = new FileWriter("./testResult2", false);
            PrintWriter pwOb = new PrintWriter(fwOb, false);
            pwOb.flush();
            pwOb.close();
            fwOb.close();
            Files.write(Paths.get("./testResult2"), "".getBytes(), StandardOpenOption.DELETE_ON_CLOSE);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void checkSomeRssToOneFileWriting(){

        firstFeed = new FeedSettingsContainer("./textRss.txt", "./testResult3", 0l, null);
        secondFeed = new FeedSettingsContainer("./textRss.txt", "./testResult3", 0l, null);
        WriteThreadWithCounter rssWriter1 = new WriteThreadWithCounter(firstFeed);
        WriteThreadWithCounter rssWriter2 = new WriteThreadWithCounter(secondFeed);

        try {
            rssWriter1.run();
            rssWriter1.run();
            String mustBe = new String(Files.readAllBytes(Paths.get("./testResult3")));
            rssWriter2.run();
            rssWriter2.run();
            String readFile = new String(Files.readAllBytes(Paths.get("./testResult3")));
            //readFile = readFile.replaceAll(mustBe, "");
            mustBe = mustBe + mustBe;
            assertEquals(mustBe, readFile);
            //Files.write(Paths.get("testResult"), new String().getBytes(), StandardOpenOption.WRITE);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}